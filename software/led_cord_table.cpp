﻿#include "led_cord_table.h"

const symbolInfo TBL[]={ 
		{12, 5, 'c', 0}, 
		{18, 6, 'h', 12}, 
		{0, 5, ' ', 30},
		{8, 2, '!', 30},
		{4, 3, '"', 38},
		{20, 5, '#', 42},
		{21, 5, '$', 62},
		{21, 5, '%', 83},
		{16, 5, '&', 104},
		{2, 1, '\'', 120}, 
		{9, 2, '(', 122},
		{9, 2, ')', 131},
		{5, 3, '*', 140},
		{9, 5, '+', 145},
		{2, 1, ',', 154},
		{5, 5, '-', 156},
		{1, 1, '.', 161},
		{13, 5, '/', 162},
		{20, 5, '0', 175},
		{15, 5, '1', 195},
		{16, 5, '2', 210},
		{18, 5, '3', 226},
		{17, 5, '4', 244},
		{18, 5, '5', 261},
		{20, 5, '6', 279},
		{13, 5, '7', 299},
		{21, 5, '8', 312},
		{20, 5, '9', 333},
		{2, 1, ':', 353},
		{3, 1, ';', 355},
		{14, 5, '<', 358},
		{10, 5, '=', 372},
		{14, 5, '>', 382},
		{11, 5, '?', 396},
		{22, 5, '@', 407},
		{22, 5, 'A', 429},
		{24, 5, 'B', 451},
		{15, 5, 'C', 475},
		{22, 5, 'D', 490},
		{21, 5, 'E', 512},
		{17, 5, 'F', 533},
		{19, 5, 'G', 550},
		{21, 5, 'H', 569},
		{13, 3, 'I', 590},
		{11, 4, 'J', 603},
		{17, 5, 'K', 614},
		{11, 4, 'L', 631},
		{28, 7, 'M', 642},
		{25, 5, 'N', 670},
		{20, 5, 'O', 695},
		{18, 5, 'P', 715},
		{23, 5, 'Q', 733},
		{22, 5, 'R', 756},
		{17, 5, 'S', 778},
		{13, 5, 'T', 795},
		{19, 5, 'U', 808},
		{17, 5, 'V', 827},
		{24, 7, 'W', 844},
		{17, 5, 'X', 868},
		{13, 5, 'Y', 885},
		{17, 5, 'Z', 898},
		{11, 2, '[', 915},
		{13, 5, '\\', 926},//Р вЂ”Р Р…Р В°Р С” "\"
		{11, 2, ']', 939},
		{5, 5, '^', 950},
		{5, 5, '_', 955},
		{2, 2, '`', 960},
		{17, 5, 'a', 962},
		{18, 5, 'b', 979},
		{11, 5, 'c', 997},
		{18, 5, 'd', 1008},
		{13, 5, 'e', 1026},
		{13, 5, 'f', 1039},
		{19, 5, 'g', 1052},
		{16, 5, 'h', 1070},
		{6, 1, 'i', 1087},
		{7, 3, 'j', 1093},
		{14, 4, 'k', 1100},
		{8, 1, 'l', 1114},
		{15, 5, 'm', 1122},
		{12, 5, 'n', 1137},
		{12, 5, 'o', 1149},
		{16, 5, 'p', 1161},
		{15, 5, 'q', 1177},
		{9, 5, 'r', 1192},
		{13, 5, 's', 1201},
		{14, 5, 't', 1214},
		{11, 5, 'u', 1228},
		{9, 5, 'v', 1239},
		{13, 7, 'w', 1248},
		{9, 5, 'x', 1261},
		{14, 5, 'y', 1270},
		{13, 5, 'z', 1284},
	  	{9, 3, '{', 1297},
		{9, 1, '|', 1306},
		{9, 3, '}', 1315},
		{5, 5, '~', 1324},
};