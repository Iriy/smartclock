﻿#include "led_cord_table_setup.h"
#include <EEPROM.h>
int address = 0;
#define END_OF_TABLE 128 - START_ANSI
void setup (){
    Serial.begin(19200); 

    
    for(int symbol = 0; symbol < END_OF_TABLE; symbol++){
        Serial.println(address);
        for(int i = 0; i < TBL[symbol].ledCount; i++){
            EEPROM.write(address, TBL[symbol].ledCord[i].x * 16 + TBL[symbol].ledCord[i].y);
            address++;
        }  
    }
}
void loop (){
}