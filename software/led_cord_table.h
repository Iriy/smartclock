﻿typedef unsigned char byte;

typedef struct {
	byte x;
	byte y;
} cord;

typedef struct {
	byte ledCount;
	byte width;
	char symbolCode;
	int startBox;
} symbolInfo;

#define EOL {255,255}
#define START_ANSI 30

extern const symbolInfo TBL[];