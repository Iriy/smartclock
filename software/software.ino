﻿#include <EEPROM.h>

#include <Wire.h>
#include "RTClib.h"
RTC_DS1307 rtc;
DateTime now; 

#include <Math.h>
#include "DHT.h"
#define DHT_PIN 2  
#define DHT_TYPE DHT22
DHT dht(DHT_PIN, DHT_TYPE);

#include "led_cord_table.h"

#define LED_WIDTH 25
#define LED_HEIGHT 9
#define LED_ON true
#define LED_OFF false
#define OUTPUT_LED_COUNT LED_WIDTH*LED_HEIGHT
#define SPACE_LENGTH 1
#define INPUT_LENGTH 5

#define DELAY 5000 // 

#define RED 5
#define GREEN 15
#define BLUE 0

#define CELSIUS 30
#define HUMIDITY 31

#define NUM_TO_CHAR 48

#include <Adafruit_NeoPixel.h>
#define DATA_PIN 6
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(OUTPUT_LED_COUNT, DATA_PIN, NEO_GRB + NEO_KHZ800);

void DisplayShow(char* input);
void initDisplay();
void ConvertInput(char *DataIn);
void MainProcess(char *DataIn);
int CordToNumberConvertor(int pos, int shift);

void TemperatureVisual();
void HumidityVisual();
void TimeVisual();
void DateVisual();

char data[INPUT_LENGTH];
typedef void(*VisualizatorDisplay)() ;

VisualizatorDisplay Visualizators[5];

void setup(){
    //Serial.begin(19200);
    if (! rtc.begin()) {
    while (1);
  }

  if ( !rtc.isrunning() ) {
    rtc.adjust(DateTime(2016, 11, 12, 12, 28, 00));
  }
  pixels.begin();
  
  Visualizators[0] = TimeVisual;
  Visualizators[1] = DateVisual;
  Visualizators[2] = TemperatureVisual;
  Visualizators[3] = HumidityVisual;
}

void loop() {
    now = rtc.now(); 
    for(int i = 0; i < 4; i++){
        Visualizators[i]();
        DisplayShow(data);
        delay(DELAY);
    }
}

void TemperatureVisual(){
    data[0] = floor(dht.readTemperature() / 10) + NUM_TO_CHAR;
    data[1] = fmod(dht.readTemperature(), 10) - fmod(dht.readTemperature(), 1) + NUM_TO_CHAR;
    data[2] = '.';
    data[3] = fmod(dht.readTemperature(), 1) * 10 + NUM_TO_CHAR;
    data[4] = CELSIUS;  
}
void HumidityVisual(){
    data[0] = HUMIDITY;
    data[1] = floor(dht.readHumidity() / 10) + NUM_TO_CHAR;
    data[2] = fmod(dht.readHumidity(), 10) - fmod(dht.readHumidity(), 1) + NUM_TO_CHAR;
    data[3] = '%';
}
void TimeVisual(){
    
      data[0] = (now.hour() - now.hour() % 10) / 10 + NUM_TO_CHAR;
      data[1] = now.hour() % 10 + NUM_TO_CHAR;
      data[2] = ':';
      data[3] = (now.minute() - now.minute() % 10) / 10 + NUM_TO_CHAR;
      data[4] = now.minute() % 10 + NUM_TO_CHAR;  
}
void DateVisual(){
    
      data[0] = (now.day() - now.day() % 10) / 10 + NUM_TO_CHAR;
      data[1] = now.day() % 10 + NUM_TO_CHAR;
      data[2] = '.';
      data[3] = (now.month() - now.month() % 10) / 10 + NUM_TO_CHAR;
      data[4] = now.month() % 10 + NUM_TO_CHAR;
}

void DisplayShow(char* input){
    initDisplay();
    ConvertInput(input);
    MainProcess(input);
}

void initDisplay() {
	for (int j = 0; j < OUTPUT_LED_COUNT; j++){
                pixels.setPixelColor(j,pixels.Color(0,0,0));
      }
}

void ConvertInput(char *DataIn) {
	for (int i = 0; i < INPUT_LENGTH; i++)
		DataIn[i] -= START_ANSI;
}

void MainProcess(char *DataIn) {
	int Shift = 0;
	for (int i = 0; i < INPUT_LENGTH && Shift < LED_WIDTH; i++) {
		for (int led = TBL[DataIn[i]].startBox; led < TBL[DataIn[i]].startBox + TBL[DataIn[i]].ledCount; led++){
			int ledNumber = CordToNumberConvertor(EEPROM.read(led), Shift);
                        pixels.setPixelColor(ledNumber, pixels.Color(RED, GREEN, BLUE));
                }
		Shift = Shift + TBL[DataIn[i]].width + SPACE_LENGTH;
	}
    pixels.show();
}

int CordToNumberConvertor(int pos, int shift) {
  int y = pos % 16;
  int x = (pos - y) / 16;
  int result = y*LED_WIDTH;
  if((y % 2) == 0)
    result += x + shift;
  else
    result += LED_WIDTH - x - 1 - shift;   
  
  return result;
}